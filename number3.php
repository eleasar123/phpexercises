<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style> 
        body{
            background-color: pink;
            text-align: center;
        }
        #table{
            width: 90vw;
            padding: 10px;
            border-radius: 5px;
            border: solid green 5px;
            display: table;
            margin-left: auto;
            margin-right: auto;
            margin-top: 50px;
            font-family: Arial, Helvetica, sans-serif;
            
        }
        tr{
            background-color: yellowgreen;
            font-size: 20px;
        }
    </style>
</head>
<body>
<table id="table">
<h1>Division Table</h1> 
<?php
//Exercie number 3
for($divisor=1;$divisor<=12;$divisor++){
    echo "<tr>";
    for($dividend=1;$dividend<=12;$dividend++){
        $dividend2=$dividend*$divisor;
        echo "<td> $dividend2/$divisor = ".(int)($dividend*$divisor)/(int)$divisor."</td>";
    }
    echo "</tr>";
}
?>
</table>
</body>
</html>
